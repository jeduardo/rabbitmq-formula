describe user('rabbitmq') do
  it { should exist }
end

describe group('rabbitmq') do
  it { should exist }
end

describe package('rabbitmq-server') do
  it { should be_installed }
end

describe service('rabbitmq-server') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
